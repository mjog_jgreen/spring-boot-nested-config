package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "replies")
public class RepliesConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(RepliesConfiguration.class);
    /**
     * See https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/jms/listener/AbstractMessageListenerContainer.html
     */
    private boolean sessionTransacted;
    /**
     * See https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/jms/config/DefaultJmsListenerContainerFactory.html#setConcurrency-java.lang.String-
     */
    private String concurrency;
    private String sessionAcknowledgementModeName;
    private Queues queues;

    public boolean isSessionTransacted() {
        return sessionTransacted;
    }

    public void setSessionTransacted(boolean sessionTransacted) {
        this.sessionTransacted = sessionTransacted;
    }

    public String getConcurrency() {
        return concurrency;
    }

    public void setConcurrency(String concurrency) {
        this.concurrency = concurrency;
    }

    public String getSessionAcknowledgementModeName() {
        return sessionAcknowledgementModeName;
    }

    public void setSessionAcknowledgementModeName(String sessionAcknowledgementModeName) {
        this.sessionAcknowledgementModeName = sessionAcknowledgementModeName;
    }

    public Queues getQueues() {
        return queues;
    }

    public void setQueues(Queues queues) {
        this.queues = queues;
    }

    private static class Queues {
        private String sms;
        private String email;
        private String internet;

        public String getSms() {
            return sms;
        }

        public void setSms(String sms) {
            this.sms = sms;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
            LOGGER.info("Received config: {}", email);
        }

        public String getInternet() {
            return internet;
        }

        public void setInternet(String internet) {
            this.internet = internet;
        }
    }
}
